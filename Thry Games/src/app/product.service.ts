import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor() { }
  
  products = [
    {
      image: 'https://www.gematsu.com/wp-content/uploads/2023/08/Lords-of-the-Fallen_2023_08-22-23_002.jpg',
      category: 'new',
      title: 'LORDS OF THE FALLEN',
      description: 'A vast world awaits in all-new, dark fantasy action-RPG, Lords of the Fallen.',
      comments: 3,
      originalPrice: 899.00, 
      discount: Math.floor(Math.random() * 30), 
    },
    {
      image: 'https://www.giantbomb.com/a/uploads/scale_medium/33/338034/3294773-4040730227-libra.jpg',
      category: 'strategy',
      title: 'Blazing Sails',
      description: 'Fight for survival in Blazing Sails, a fast-paced pirate PvP game! Create your own unique pirate and ship.',
      comments: 1,
      originalPrice: 799.00, 
      discount: 100, 
    },
    {
      image: 'http://t0.gstatic.com/images?q=tbn:ANd9GcSV3wLoFOAuXG9j6WFevdWx71raH_dboK5CX5hbQ-U7myHFxMpLEnPob4ISiDbcSv44Dx64',
      category: 'new',
      title: 'Dead by Daylight',
      description: 'Dead by Daylight is a multiplayer (4vs1) horror game where one player takes on the role of the savage Killer.',
      comments: 5,
      originalPrice: 300.00, 
      discount: Math.floor(Math.random() * 30), 
    },
    {
      image: 'https://wallpapercg.com/media/ts_orig/2723.webp',
      category: 'racing',
      title: 'Need for Speed™ Unbound',
      description: 'Be notorious on the streets of Lakeshore, as Need for Speed™ Unbound Vol 5 brings new ways to earn XP and Bank..',
      comments: 3,
      originalPrice: 300.00, 
      discount: Math.floor(Math.random() * 30), 
    },
    {
      image: 'https://wallpapercg.com/media/ts_orig/546.webp',
      category: 'story',
      title: 'Assassins Creed',
      description: 'You are an Assassin, a warrior shrouded in secrecy and feared for your ruthlessness. ',
      comments: 3,
      originalPrice: 300.00, 
      discount: Math.floor(Math.random() * 30), 
    },
    {
      image: 'https://wallpapercg.com/media/ts_orig/11848.webp',
      category: 'Adventure',
      title: 'Grand Theft Auto V',
      description: 'The Grand Theft Auto V: Premium Edition includes the complete GTAV story ',
      comments: 3,
      originalPrice: 300.00, 
      discount: Math.floor(Math.random() * 30), 
    },
    {
      image: 'https://c4.wallpaperflare.com/wallpaper/396/109/160/e3-2017-poster-the-evil-within-2-8k-wallpaper-preview.jpg',
      category: 'Detactive',
      title: 'The Evil Within',
      description: 'Developed by Tango Gameworks and directed by Resident Evil series creator Shinji Mikami',
      comments: 3,
      originalPrice: 300.00, 
      discount: Math.floor(Math.random() * 30), 
    },
    {
      image: 'https://wallpapercg.com/media/ts_2x/2642.webp',
      category: 'Shooting',
      title: 'Fortnite',
      description: 'Grab all of your friends and drop into Epic Games Fortnite, a massive 100-player face-off that combines looting, crafting, shootouts and chaos.',
      comments: 3,
      originalPrice: 300.00, 
      discount: Math.floor(Math.random() * 30), 
    },
    {
      image: 'https://wallpapercg.com/media/ts_orig/6178.webp',
      category: 'Adventure',
      title: 'SpiderMan',
      description: 'In Marvel’s Spider-Man Remastered, the worlds of Peter Parker and Spider-Man collide in an original.',
      comments: 3,
      originalPrice: 300.00, 
      discount: Math.floor(Math.random() * 30), 
    },
    {
      image: 'https://images.gog-statics.com/5c1d500c9af36ffebf49f5b1c95be2a58c156ed6b49dbf4dd97a7e6f0d765de9_product_card_v2_mobile_slider_639.jpg',
      category: 'Adventure',
      title: 'Night Loops',
      description: 'A journey through the psyche. Escape your night shift at the hotel, explore dreamlike worlds',
      comments: 3,
      originalPrice: 300.00, 
      discount: Math.floor(Math.random() * 30), 
    },

    {
      image: 'https://www.thegamedetective.com/cdn/shop/products/Outlive-Collector_sEdition_1024x1024.jpg?v=1612494743',
      category: 'Horror',
      title: 'Outlive',
      description: 'Outlive is an intense game of survival! Run, hide and outsmart mutants in a post-apocalyptic world.',
      comments: 3,
      originalPrice: 300.00, 
      discount: Math.floor(Math.random() * 30), 
    },
    {
      image: 'https://images.pushsquare.com/aa216e1709e8c/1280x720.jpg',
      category: 'Fighting',
      title: 'The Crown of Wu',
      description: 'Unravel the mystery behind the stolen crown of Wu. Run, jump, fight, explore, master the elements of fire, air, earth and lightning, cross through chasms',
      comments: 3,
      originalPrice: 300.00, 
      discount: Math.floor(Math.random() * 30), 
    },
    {
      image: 'https://media.indiedb.com/images/members/5/4405/4404417/profile/Kickstarter_live.jpeg',
      category: 'Action-Adventure',
      title: 'Farsiders',
      description: 'Embark on a journey between past and present in a world filled with technology and magic.',
      comments: 3,
      originalPrice: 300.00, 
      discount: Math.floor(Math.random() * 30), 
    },
    {
      image: 'https://upload.wikimedia.org/wikipedia/en/7/7c/Payday_3_cover_art.jpg',
      category: 'shooter',
      title: 'PAYDAY 3',
      description: 'PAYDAY 3 is the explosive sequel to one of the most popular co-op shooters of the past decade.',
      comments: 3,
      originalPrice: 300.00, 
      discount: Math.floor(Math.random() * 30), 
    },
    {
      image: 'https://cdn-wp.thesportsrush.com/2020/09/30c8e13ccd88ba03757aeba0be7b5f9d.png',
      category: 'Card Game',
      title: 'Brawlers',
      description: 'Introducing Brawlers™ – the ultimate and electrifying gaming sensation thats taking the gaming world by storm!',
      comments: 3,
      originalPrice: 300.00, 
      discount: Math.floor(Math.random() * 30), 
    },
    {
      image: 'https://cdn.akamai.steamstatic.com/steam/apps/2286560/capsule_616x353.jpg?t=1693508450',
      category: 'Fighting',
      title: 'Battle of Guardians',
      description: 'Forgotten Guardians returned to save humanity from a demonic invasion.',
      comments: 3,
      originalPrice: 300.00, 
      discount: Math.floor(Math.random() * 30), 
    },
    {
      image: 'https://assets-prd.ignimgs.com/2022/11/30/raid-shadow-legends-button-1-1669836166576.jpg',
      category: 'Action',
      title: 'Raid: Shadow Legends',
      description: 'Collect over 400 Champions and take down your opponents in Raid: Shadow Legends. Explore 1+ million Champion builds in this dark fantasy Collection MMORPG!',
      comments: 3,
      originalPrice: 300.00, 
      discount: Math.floor(Math.random() * 30), 
    },
    {
      image: 'https://cdn1.epicgames.com/offer/d0230ef21b1f4ef29aec0d49946912fc/TL1_PDP_Banner_1440_2560x1440-bf1e4c7ccea4b1f48cba1c014d8a85e8',
      category: 'Dungeon Crawler',
      title: 'Torchlight',
      description: 'Legions of twisted creatures, emboldened by Ember, have begun to swarm up from the caves below the sleepy enclave of Torchlight.',
      comments: 3,
      originalPrice: 300.00, 
      discount: Math.floor(Math.random() * 30), 
    },
    {
      image: 'https://assets.nintendo.com/image/upload/c_fill,w_1200/q_auto:best/f_auto/dpr_2.0/ncom/en_US/games/switch/o/operencia-the-stolen-sun-switch/hero',
      category: 'Exploration',
      title: 'Operencia',
      description: 'Your own personal treasure chest of Operencia loot: FULL SOUNDTRACK: Crafted by accomplished Hungarian composer Arthur Grósz.',
      comments: 3,
      originalPrice: 300.00, 
      discount: Math.floor(Math.random() * 30), 
    },
    {
      image: 'https://i0.wp.com/uploads.freegogpcgames.com/image/Time-Commando.jpg?fit=678%2C381&ssl=1',
      category: 'Fighting',
      title: 'Time Commando',
      description: 'For retro gaming lover, discover a jewel of 1996: Time Commando Lets fight across 9 time periods: prehistoric, Roman, feudal Japan, Medieval, Conquistador, the Wild West, and the modern wars',
      comments: 3,
      originalPrice: 300.00, 
      discount: Math.floor(Math.random() * 30), 
    },
  ];


  getProducts(): any[] {
    return this.products;
  }


}
