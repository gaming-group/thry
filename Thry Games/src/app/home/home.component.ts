import { Component, NgZone, OnInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import * as $ from 'jquery';
import 'jquery.marquee';
import { ThryService } from '../thry.service';
import { ProductService } from '../product.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  showNavigationArrows = true;
  showNavigationIndicators = true;
  
  images = [
    'https://c4.wallpaperflare.com/wallpaper/913/884/836/fortnite-pc-gaming-game-logo-wallpaper-preview.jpg',
    'https://c4.wallpaperflare.com/wallpaper/1017/397/309/michael-grand-theft-auto-v-franklin-trevor-wallpaper-preview.jpg',
    'https://c4.wallpaperflare.com/wallpaper/169/715/816/pubg-playerunknowns-battlegrounds-2018-games-games-wallpaper-preview.jpg',
    'assets/img/slider-1.jpg', 'assets/img/slider-2.jpg'
  ];
  
 
  recentGames = [
    {
      background: 'assets/img/recent-game-bg.png',
      category: 'new',
      title: 'LORDS OF THE FALLEN ',
      description: ' A vast world awaits in all-new, dark fantasy action-RPG, Lords of the Fallen.  ',
      comments: 3,
      thumb: 'https://www.gematsu.com/wp-content/uploads/2023/08/Lords-of-the-Fallen_2023_08-22-23_002.jpg',
    },
    {
      background: 'assets/img/recent-game-bg.png',
      category: 'racing',
      title: 'HOT WHEELS UNLEASHED™ -2',
      description: 'Lorem ipsum dolor sit amet, consectetur adipisc ing ipsum dolor sit amet, consectetur elit.',
      comments: 3,
      thumb: 'https://cdn.cloudflare.steamstatic.com/steam/apps/2051120/capsule_616x353.jpg?t=1694793100g',
    },
    {
      background: 'assets/img/recent-game-bg.png',
      category: 'adventure',
      title: 'Colossal Cave',
      description: 'Acclaimed game designer Roberta Williams revives Colossal Cave as a retro, point-&-click',
      comments: 3,
      thumb: 'https://www.gematsu.com/wp-content/uploads/2022/08/Colossal-Cave-Switch_08-29-22.jpg',
    },
  ];

  recentReviews = [
    {
      background: 'assets/img/review-bg.png',
      category: 'new',
      title: "Assasin's Creed",
      score: 9.3,
      description: 'Unveil dark secrets and forgotten myths as you go back to the one founding moment: The Origins of the Assassin’s Brotherhood.',
      cover: 'assets/img/review/1.jpg',
    },
    {
      background: 'assets/img/review-bg.png',
      category: 'new',
      title: 'Doom',
      score: 9.5,
      description: 'You’re a marine—one of Earth’s best—recently assigned to the Union Aerospace Corporation (UAC) research facility on Mars.',
      cover: 'assets/img/review/2.jpg',
    },
    {
      background: 'assets/img/review-bg.png',
      category: 'new',
      title: 'Overwatch',
      score: 9.1,
      description: 'Santa has been kidnapped and Holidays are at stake. ',
      cover: 'assets/img/review/3.jpg',
    },
    {
      background: 'assets/img/review-bg.png',
      category: 'new',
      title: 'GTA',
      score: 9.7,
      description: 'The Grand Theft Auto V: Premium Edition includes the complete GTAV story, Grand Theft Auto Online and all existing gameplay upgrades and content. ',
      cover: 'assets/img/review/4.jpg',
    },
  ];

  products: any[] = [];
  originalProducts: any[] = []; // Initialize as an empty array
  constructor(config: NgbCarouselConfig, private router: Router,private service :ThryService,
    private productsService:ProductService,private http: HttpClient) {
    config.showNavigationArrows = true;
    config.showNavigationIndicators = true;
    config.interval = 5000;
    config.wrap = true;
  }

  ngOnInit() {
    this.products = this.productsService.getProducts();
  }

  selectedCard: any; 
  showCardDetails(product: any) {
    // Store the selected product in localStorage
    localStorage.setItem('selectedProduct', JSON.stringify(product));
  
    // Navigate to the CartComponent
    this.router.navigate(['game']);

}


}
