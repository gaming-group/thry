import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ThryService } from '../thry.service';
import { Router } from '@angular/router';
import { MatDatepickerModule } from '@angular/material/datepicker';





@Component({
  selector: 'app-sing-up',
  templateUrl: './sing-up.component.html',
  styleUrls: ['./sing-up.component.css']
})
export class SingUpComponent implements OnInit {
  customer: any;
  coutriesList : any;

  password:string = '';
  Repassword:string = '';

  customers:any;

  showDateDropdown: boolean = false;
  showMonthDropdown: boolean = false;
  showYearDropdown: boolean = false;

  showError: boolean = false;

  currentStep: 'dob' | 'signup' ;

  constructor(private toaster:ToastrService,private router : Router,private service : ThryService) {


    this.currentStep='dob';
    this.customer = {
      
      "custName": "",
      "gender": "",
      "country": "",
      "emailId": "",
      "age":"",
      "mobile":"",
      "password": ""
      }
  }

  selectedDate: number = 1; 
  selectedMonth: number = 1; 
  selectedYear: number = new Date().getFullYear();
  formattedDateOfBirth: string = ''; 

  dates: number[] = Array.from({ length: 31 }, (_, i) => i + 1);
  months = [
    { name: 'January', value: 1 },
    { name: 'February', value: 2 },
    { name: 'March', value: 3 },
    { name: 'April', value: 4 },
    { name: 'May', value: 5 },
    { name: 'June', value: 6 },
    { name: 'July', value: 7 },
    { name: 'August', value: 8 },
    { name: 'September', value: 9 },
    { name: 'October', value: 10 },
    { name: 'November', value: 11 },
    { name: 'December', value: 12 },
  ];
  
  currentYear = new Date().getFullYear();
  years: number[] = Array.from({ length: 100 }, (_, i) => this.currentYear - 50 + i);

  toggleDropdowns() {
    this.showDateDropdown = true;
    this.showMonthDropdown = true;
    this.showYearDropdown = true;
  }

  checkAgeAndRedirect() {
    console.log('checkAgeAndRedirect called');
    const currentDate = new Date();
    const selectedBirthDate = new Date(
      this.selectedYear,
      this.selectedMonth - 1, 
      this.selectedDate
    );
    const age = currentDate.getFullYear() - selectedBirthDate.getFullYear();
  
    if (age < 18) {
      this.toaster.error("Sorry for the inconvenience");
      this.showError = true;
      console.log('vinay');
    } else {
      this.formattedDateOfBirth = `${this.selectedYear}-${this.selectedMonth.toString().padStart(2, '0')}-${this.selectedDate.toString().padStart(2, '0')}`;
      this.currentStep = 'signup';
      // this.toaster.success('Lets Get Explore Thry Games');
    }
  }
  

  ngOnInit(){
    this.service.getCountries().subscribe((countriesData: any) => {this.coutriesList = countriesData;});
    this.service.getAllCustomers().subscribe(data => {console.log(data)});
  }

  
  
  customerRegister(regForm: any) {
    if (this.customer.Repassword !== this.customer.password) {
      this.toaster.error('Passwords do not match');
    } else {
      this.service.checkIfEmailExists(regForm.emailId).subscribe((emailExists: boolean) => {
        if (emailExists) {
          this.toaster.error('EmailId is already in use');
        } else {
          this.customer.name = regForm.custName;
          this.customer.gender = regForm.gender;
          this.customer.country = regForm.country;
          this.customer.email = regForm.emailId;
          this.customer.dateOfBirth = regForm.age;
          this.customer.mobileNo = regForm.mobile;
          this.customer.password = regForm.password;
  
          this.service.register(this.customer).subscribe((data: any) => {
            console.log(data);
          });
          this.toaster.success('Registered Successfully');
          this.router.navigate(['signin']);
        }
      });
    }
  }
  
  
  validateMail(mail:any):boolean{
    return /[@]/.test(mail);
  }
  validateMobile(mobile: string): boolean {
    return mobile.length == 10 && /[6-9]/.test(mobile[0]);
  }
  validatePassword(password: string): boolean {
    return password.length >= 3 && /[A-Z]/.test(password) && /[0-9]/.test(password) && /[!@#$%^&*()_+{}\[\]:;<>,.?~\\\-]/.test(password);
  }

  
}
