import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ThryService } from '../thry.service';

@Component({
  selector: 'app-sign-out',
  templateUrl: './sign-out.component.html',
  styleUrls: ['./sign-out.component.css']
})
export class SignOutComponent {

  constructor(private router: Router, private service: ThryService) {
  }
  ngOnInit() {
    this.service.setUserLoggedOut();
    this.router.navigate(['signin']);
    window.history.pushState(null, '', '/signin');
  }
}
