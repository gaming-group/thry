import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { ThryService } from '../thry.service';
import { ToastrService } from 'ngx-toastr';
import { HttpClient } from '@angular/common/http';

declare var Razorpay:any;

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css'],
})
export class GameComponent implements OnInit {

  showNavigationArrows = true;
  showNavigationIndicators = true;

  images = [
    'https://images.hdqwalls.com/wallpapers/bthumb/lords-of-the-fallen-2023-4k-ea.jpg',
    'https://images.hdqwalls.com/wallpapers/thumb/lords-of-the-fallen.jpg',
    'https://images.hdqwalls.com/wallpapers/bthumb/the-lords-of-the-fallen-5k-4e.jpg',
    'https://upload.wikimedia.org/wikipedia/commons/c/cc/Champs_elysees.700px.jpg',
  ];

  selectedProduct: any;
  loginStatus: any;
  products: any;
  temp: any;
  image: any;

  private paymentAmount: number = 0;  
  displayPaymentDialog: boolean = false;

  constructor(config: NgbCarouselConfig,private router:Router,private service:ThryService,private toaster:ToastrService,private http: HttpClient) {
    config.showNavigationArrows = true;
    config.showNavigationIndicators = true;
    config.interval = 5000;
    config.wrap = true;

    this.temp = localStorage.getItem('selectedProduct');
    this.products = JSON.parse(this.temp);
    this.loginStatus = this.service.getUserLoggedStatus();

    
  }

  ngOnInit(): void {
     // Retrieve the selected product from localStorage
     const storedProduct = localStorage.getItem('selectedProduct');

     if (storedProduct) {
       this.selectedProduct = JSON.parse(storedProduct);
     }
 
  }


  buynow(product: any) {
    this.image = product.image;
    if (this.loginStatus) {
      if (this.image) {
        const discountedPrice = product.originalPrice * (1 - product.discount / 100);
        if (discountedPrice > 0) {
          // Ask for payment
          console.log('from if condition')
          this.paymentAmount = discountedPrice;
          // this.displayPaymentDialog = true;
          this.paynow();
        } else {
          // Download directly without payment
          this.downloadImage(this.image);
        }
      } else {
        this.toaster.error('Image URL is not defined.');
      }
    } else {
      this.toaster.error('User Must Sign In To Download');
      this.router.navigate(['signin']);
    }
  }

  downloadImage(imageUrl: string) {
    const filename = 'image.jpg';
  
    this.http.get(imageUrl, { responseType: 'blob' }).subscribe((data) => {
      const blob = new Blob([data], { type: 'image/jpeg' });
      const url = window.URL.createObjectURL(blob);
      const a = document.createElement('a');
      a.href = url;
      a.download = filename;
      a.click();
      window.URL.revokeObjectURL(url);
    });
  }


  paynow() {
    if (this.paymentAmount > 0) {
      console.log('from paynow() method')
      const RozarpayOptions = {
        description: 'Thry Payment',
        currency: 'INR',
        amount: this.paymentAmount * 100, 
        name: 'Thry Games',
        key: 'rzp_test_Lbm183bcJ9aYg6',
        image: '../../assets/img/Thry.png',
        prefills: {
          name: 'vinay kumar',
          email: 'vinaykumarkompelli27@gmail.com',
          phone: '7729085121',
        },
        theme: {
          color: 'rgb(79, 75, 75)',
        },
        modal: {
          ondismiss: () => {
            console.log('dismissed');
          },
        },
      };

      const successCallback = (paymentid: any) => {
        this.downloadImage(this.image);
        console.log('Payment successful: ' + paymentid);
      };      

      const failureCallback = (e: any) => {
        console.log('Payment failed: ' + e);
        this.toaster.error('Payment failed');
      };

      Razorpay.open(RozarpayOptions, successCallback, failureCallback);
    }
  }
}
