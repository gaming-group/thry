package com.dao;

import java.time.LocalDateTime;

import javax.persistence.Id;

import org.springframework.stereotype.Component;

@Component
public class CustomerOtp {

	@Id
	private int emailOtp;
	private LocalDateTime emailOtpExpiryTime;
	private int mobileOtp;
	private LocalDateTime mobileOtpExpiryTime;
	public int getEmailOtp() {
		return emailOtp;
	}
	public void setEmailOtp(int emailOtp) {
		this.emailOtp = emailOtp;
	}
	
	public LocalDateTime getEmailOtpExpiryTime() {
		return emailOtpExpiryTime;
	}
	public void setEmailOtpExpiryTime(LocalDateTime emailOtpExpiryTime) {
		this.emailOtpExpiryTime = emailOtpExpiryTime;
	}
	public int getMobileOtp() {
		return mobileOtp;
	}
	public void setMobileOtp(int mobileOtp) {
		this.mobileOtp = mobileOtp;
	}
	
	public LocalDateTime getMobileOtpExpiryTime() {
		return mobileOtpExpiryTime;
	}
	public void setMobileOtpExpiryTime(LocalDateTime mobileOtpExpiryTime) {
		this.mobileOtpExpiryTime = mobileOtpExpiryTime;
	}	
}